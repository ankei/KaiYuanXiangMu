package com.winway.jxl.util;

public class Loger {
	static boolean isLoger = true;
	static boolean isException = true;
	static boolean isError = true;

	public static void openLoger(boolean loger, boolean exception, boolean error) {
		isLoger = loger;
		isException = exception;
		isError = error;
	}

	public static <T> void log(T msg) {
		if (!isLoger) {
			return;
		}
		System.out.println(msg);
	}

	public static <T> void log(T msg, boolean log) {
		if (log) {
			log(msg);
		}
	}

	public static <T> void log(String tag, T msg) {
		if (tag.length() < 10) {
			tag = tag + "          ";
			tag = tag.substring(0, 10);
		}
		String message = tag + " : " + msg;
		log(message);
	}

	public static <T> void log(String tag, T msg, boolean log) {
		if (log) {
			log(tag, msg);
		}
	}

	public static void log(Throwable ex) {
		if (isException) {
			ex.printStackTrace();
		}
	}

	public static void log(Throwable ex, boolean log) {
		if (log) {
			log(ex);
		}
	}

	public static <T> void error(T error) {
		if (isError) {
			System.err.println(error);
		}
	}

	public static <T> void error(T error, boolean log) {
		if (log) {
			error(error);
		}
	}
}
