package com.winway.jxl.anomotion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CellEntity {
	/**
	 * 判断对象是否要写入Excel中
	 * 
	 * @return
	 */
	boolean excel() default true;

	/**
	 * 偏移量
	 * 
	 * @return
	 */
	int offset() default 0;
}
