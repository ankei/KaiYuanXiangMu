package com.winway.jxl.anomotion;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
public @interface CellColum {
	/**
	 * 表头所在列序号
	 * @time 2018年1月15日 下午3:18:02
	 * @return
	 */
	int index() default -1;

	/**
	 * 表头名称
	 * @time 2018年1月15日 下午3:17:53
	 * @return
	 */
	String headerName() default "";

	/**
	 * 
	 * @time 2018年1月15日 下午3:17:49
	 * @return
	 */
	boolean wrap() default false;

	/**
	 * 表格列的宽度
	 * @return
	 */
	int lenth() default 10;

	/**
	 * 表头别名（在读取Excel的时候，如果headerName表头找不到，则使用otherHeaderName进行读取）
	 * @time 2018年1月15日 下午3:18:36
	 * @return
	 */
	String[] otherHeaderNames() default "";
}
