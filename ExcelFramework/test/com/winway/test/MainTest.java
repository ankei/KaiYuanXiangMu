package com.winway.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.winway.jxl.core.ExcelReader;
import com.winway.jxl.core.ExcelWriter;

public class MainTest {

	public static void main(String[] args) throws Exception {
		writeTest();
		readTest();
	}

	private static void readTest() throws Exception {
		ExcelReader<BookEntity> reader = new ExcelReader<>(BookEntity.class, "测试生成Excel.xls");
		List<BookEntity> list = reader.readExcel(0, 0);
		for (BookEntity bookEntity : list) {
			System.out.println(bookEntity);
		}
	}

	private static void writeTest() throws Exception {
		ArrayList<BookEntity> bookList = new ArrayList<>();
		bookList.add(new BookEntity("十万个为什么", "小明", 10.8f, "中国科学出版社", new Date(), 30));
		bookList.add(new BookEntity("十万个为什么", "小明", 10.8f, "中国科学出版社", new Date(), 30));
		bookList.add(new BookEntity("十万个为什么", "小明", 10.8f, "中国科学出版社", new Date(), 30));
		bookList.add(new BookEntity("李白诗集", "李白", 100, "中国科学出版社", new Date(), 100));
		ExcelWriter<BookEntity> writer = new ExcelWriter<>(new File("测试生成Excel.xls"));
		writer.writeExcel(bookList, "图书");
		System.out.println("END");
	}
}
