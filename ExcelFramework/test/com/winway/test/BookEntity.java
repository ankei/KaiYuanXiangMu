package com.winway.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.winway.jxl.anomotion.CellColum;

public class BookEntity {
	@CellColum(headerName = "书名", index = 0, lenth = 13)
	private String bookname;
	@CellColum(headerName = "作者", index = 1, lenth = 8)
	private String authorname;
	@CellColum(headerName = "价格", index = 2, lenth = 8)
	private float price;
	@CellColum(headerName = "出版社", index = 3, lenth = 13)
	private String publishCompony;
	@CellColum(headerName = "出版日期", index = 4, lenth = 13)
	private Date publishDate;
	@CellColum(headerName = "页数", index = 5, lenth = 8)
	private int pageCount;

	public BookEntity() {
		super();
	}

	public BookEntity(String bookname, String authorname, float price, String publishCompony, Date publishDate,
			int pageCount) {
		super();
		this.bookname = bookname;
		this.authorname = authorname;
		this.price = price;
		this.publishCompony = publishCompony;
		this.publishDate = publishDate;
		this.pageCount = pageCount;
	}

	public String getBookname() {
		return bookname;
	}

	public void setBookname(String bookname) {
		this.bookname = bookname;
	}

	public String getAuthorname() {
		return authorname;
	}

	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getPublishCompony() {
		return publishCompony;
	}

	public void setPublishCompony(String publishCompony) {
		this.publishCompony = publishCompony;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	static String getDateString(Date date) {
		if (date == null) {
			return null;
		}
		return sdf.format(date);
	}

	@Override
	public String toString() {
		return "BookEntity [bookname=" + bookname + ", authorname=" + authorname + ", price=" + price
				+ ", publishCompony=" + publishCompony + ", publishDate=" + getDateString(publishDate) + ", pageCount="
				+ pageCount + "]";
	}

}
